// module.exports = function (browser) {};
async function getRequest(req, res, browser) {
  const page = await browser.newPage();

  await page.setDefaultNavigationTimeout(0);
  await page.setDefaultTimeout(0);
  await page.setUserAgent(
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36"
  );
  await page.goto("https://tokopedia.com/unilever", {
    waitUntil: "domcontentloaded",
  });
  await page.on("response", async function (response) {
    if (response.url().indexOf(`https://gql.tokopedia.com`) !== -1) {
      let dataJson = await response.json();
      dataJson = dataJson[0].data;
      console.log(dataJson);
    }
  });

  setTimeout(async function () {
    await page.close();
  }, 10000);
}

module.exports = { getRequest };
