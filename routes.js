const tokpedCrawler = require("./controllers/tokpedCrawler");

module.exports = function (app, browser) {
  app.get("/", function (req, res, next) {
    tokpedCrawler.getRequest(req, res, browser);
  });
};
